<?php

namespace Drupal\cfrop\Hub;

interface CfropHubInterface {

  /**
   * @param array $conf
   *
   * @return \Drupal\cfrop\Operation\OperationInterface
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function confGetOperation(array $conf);

}
