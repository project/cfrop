<?php

namespace Drupal\cfrop\Operation;

interface OperationInterface {

  /**
   * Runs the operation. Returns nothing.
   */
  public function execute();

}
